package pl.piotrolech;

public class App {
    public static void main(String[] args) {
        BaseballPlayer piotr = new BaseballPlayer("Piotr");
        FootballPlayer jakub = new FootballPlayer("Jakub");
        BaseballPlayer marcin = new BaseballPlayer("marcin");

        Team<BaseballPlayer> bluejays = new Team<>("Bluejays");
        bluejays.addPlayer(piotr);

        Team<BaseballPlayer> slaskWroclaw = new Team<>("Slask Wroclaw");
        slaskWroclaw.addPlayer(marcin);

        Team<BaseballPlayer> mapleleaves = new Team<>("Maple leaves");

        BaseballPlayer fifi = new BaseballPlayer("Fifi");
        bluejays.addPlayer(fifi);
        BaseballPlayer teodor = new BaseballPlayer("Teodor");
        bluejays.addPlayer(teodor);

        System.out.println(bluejays.numPlayers());

        bluejays.matchResult(slaskWroclaw, 1, 0);
        bluejays.matchResult(slaskWroclaw, 8, 8);
        bluejays.matchResult(slaskWroclaw, 8, 3);
        mapleleaves.matchResult(slaskWroclaw, 0, 0);

        System.out.println(bluejays.getName() + " ranking points: " + bluejays.ranking());
        System.out.println(slaskWroclaw.getName() + " ranking points " + slaskWroclaw.ranking());

        System.out.println(bluejays.compareTo(slaskWroclaw));
        System.out.println(slaskWroclaw.compareTo(bluejays));

        League<Team<BaseballPlayer>> league = new League<>("Toronto League");
        league.add(slaskWroclaw);
        league.add(bluejays);
        league.add(mapleleaves);

        league.showLeagueTable();
    }

}
