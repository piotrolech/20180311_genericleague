package pl.piotrolech;

import java.util.ArrayList;

public class Team<T extends Player> implements Comparable<Team<T>> {
    private String name;
    private int playedGames = 0;
    private int wonGames = 0;
    private int lostGames = 0;
    private int tiedGames = 0;

    private ArrayList<T> members = new ArrayList<>();

    public Team(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean addPlayer(T player) {
        if (player == null) {
            System.out.println("The player can not be null");
            return false;
        } else {
            if (this.members.contains(player)) {
                System.out.println(player.getName() + " has already been picked to this team");
                return false;
            } else {
                this.members.add(player);
                System.out.println("Player " + player.getName() + " picked to team " + this.getName());
                return true;
            }
        }
    }

    public int numPlayers() {
        return this.members.size();
    }

    public void matchResult(Team<T> opponent, int teamScore, int opponentScore) {
        String message;
        if (teamScore > opponentScore) {
            wonGames++;
            message = " beat ";
        } else if (teamScore == opponentScore) {
            tiedGames++;
            message = " drew with ";
        } else {
            lostGames++;
            message = " lost to ";
        }
        playedGames++;
        if (opponent != null) {
            System.out.println(this.getName() + message + opponent.getName());
            opponent.matchResult(null, opponentScore, teamScore);
        }
    }

    public int ranking() {
        return wonGames * 2 + tiedGames;
    }

    @Override
    public int compareTo(Team<T> team) {
        if (this.ranking() > team.ranking()) return -1;
        else if (this.ranking() < team.ranking()) return 1;
        else return 0;
    }
}
