package pl.piotrolech;

import java.util.ArrayList;

public class TestApp {
    public static void main(String[] args) {
        ArrayList<Integer> items = new ArrayList<>();

        items.add(1);
        items.add(2);
        items.add(3);
        items.add(4);
        items.add(5);

        printDoubles(items);
    }

    private static void printDoubles(ArrayList<Integer> list) {
        for (int o : list) {
            System.out.println(o * 2);
        }
    }
}
